clear; close all;

n_elements = 10;
x_left = 0;
x_right = 1;
x_elements = linspace(x_left,x_right,n_elements);
dx = mean(diff(x_elements));
dt = dx^2/10;%Should be scaled by Diffusion coefficient. =1 now
tend = dt*500;

Stiffness = sparse(2:n_elements,1:n_elements-1,-1,n_elements,n_elements)+...
    sparse(1:n_elements-1,2:n_elements,-1,n_elements,n_elements)+...
    sparse(1:n_elements,1:n_elements,2,n_elements,n_elements);
Stiffness(1,1)=2;%Singular if this is 1...   
Stiffness(end,end)=2;
Stiffness = Stiffness./dx;
% full(Stiffness)

Mass = sparse(2:n_elements,1:n_elements-1,1,n_elements,n_elements)+...
    sparse(1:n_elements-1,2:n_elements,1,n_elements,n_elements)+...
    sparse(1:n_elements,1:n_elements,4,n_elements,n_elements);
Mass(1,1)=4;
Mass(end,end)=4;
Mass = Mass;%.*dx./6
% full(Mass)

IC = exp(-(x_elements-(x_right+x_left)/2).^2/.1^2);
sol = IC';

plot(x_elements,sol,'b.--')
pause(.1)
for it = 1:round(tend/dt)
    ddt = mldivide(Mass,-Stiffness*sol);
    sol = ddt.*dt + sol;
    plot(x_elements,sol,'b.--')
    title(it)
    pause(.1)
end


